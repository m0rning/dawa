<?php

namespace frontend\models;

use Yii;
use yii\web\UploadedFile;

/**
 * This is the model class for table "record".
 *
 * @property integer $id
 * @property string $title
 * @property string $img
 * @property string $description
 */
class Record extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'record';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'img', 'description'], 'required'],
            [['description'], 'string'],
            [['title'], 'string', 'max' => 20],
             [['img'], 'file', 'skipOnEmpty' => false, 'extensions' => 'png, jpg'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'img' => 'Img',
            'description' => 'Description',
        ];
    }

    public function upload()
   {
       if ($this->validate()) {
           $this->img->saveAs('../uploads/' . $this->img->baseName . '.' . $this->img->extension);
           return true;
       } else {
           return false;
       }
   }
}
