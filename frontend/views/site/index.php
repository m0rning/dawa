<?php

use yii\helpers\Url;
/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>
<div class="site-index">

    <div class="body-content">
        <div class="heading-info">
          <h1 class="text-center">
            Приветствуем на сайте компании "Алви Трейд"
          </h1>
        </div>
        <div class="container">
          <div class="row">
              <h2>Топ публикаций :</h2>
              <?php
                // var_dump($posts);
                foreach ($posts as $post ) {
                  echo "<div class='col-md-4 no-padding record-wrap'>
                          <img class='image' src='../uploads/$post->img' />
                          <h4>$post->title</h4>
                          <p>".mb_substr($post->description,0,200,"UTF-8")."..."."</p>
                          <a class='btn btn-md btn-success pull-right' href='".Url::to(['/site/record', 'id' => $post->id])."'>Read more</a>
                        </div>";
                }
              ?>



          </div>
        </div>


    </div>
</div>

<?php
echo \yii\widgets\LinkPager::widget([
  'pagination'=>$provider->pagination,
]);
 ?>
