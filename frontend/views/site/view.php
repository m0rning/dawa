<?php
use yii\helpers\Url;
?>


  <div class="row" style="margin-top:50px">
      <div class="col-md-6">
        <img src="../uploads/<?= $model->img ?>" style="width:500px;height:500px;" alt="" />
      </div>
      <div class="col-md-6">
        <h2><?= $model->title ?></h2>
        <p>
          <?= $model->description ?>
        </p>
        <a class='btn btn-md btn-primary' href="<?= Url::to(['/site/index']) ?>">Go back</a>
      </div>


  </div>
