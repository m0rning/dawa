<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'About';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about">
  <td valign="top" style="width:530px">
<div id="g_D0A0F6B960324D418A4885B8CD98F4EF" class="AspNet-WebPartZone-Vertical">

					<div class="AspNet-WebPart">


<meta http-equiv="Content-Language" content="en-us">
<style type="text/css">
.style1 {
	font-size: medium;
}
.style2 {
	font-size: small;
}
.style5 {
	font-size: large;
	color: #686868;
}
</style>


<table style="text-align: justify; width: 760px">
	<tbody><tr>
		<td>
		<p>
		<img src="http://www3.syngenta.com/country/ua/uk/aboutcompany/PublishingImages/KSV_2015/ksv_1.jpg" width="760" height="200"></p>
		<p>&nbsp;</p>
		<p class="style1">«СИНГЕНТА» — ОДНА З ПРОВІДНИХ СВІТОВИХ КОМПАНІЙ, ЯКА
		ОБ’ЄДНУЄ ПОНАД 28 000 ФАХІВЦІВ У 90 КРАЇНАХ ЗАДЛЯ ДОСЯГНЕННЯ ОДНІЄЇ
		МЕТИ: РОЗКРИТИ ПОТЕНЦІАЛ РОСЛИНИ.</p>
		<p class="style1">&nbsp;</p>
		<p class="style2">Від часу свого заснування «Сингента» досягла значних
		успіхів і на сьогоднішній день є одним із лідерів світового агробізнесу,
		який оцінюється у понад 60 мільярдів доларів. Поточнення: увесь світовий
		агробізнес оцінюється у 60 мільярдів доларів чи ця цифра стосується лише
		«Сингенти»? Якщо йдеться про весь світовий бізнес, то речення
		переробляти не треба. До портфоліо «Сингенти» входить понад 80 засобів
		захисту рослин, а також велика кількість гібридів і сортів насіння
		польових та овочевих культур. В українському відділенні компанії працює
		більше 300 фахівців, серед яких багато кандидатів сільськогосподарських
		наук. Метою компанії «Сингента» в Україні є пропозиція комплексних
		рішень для українських аграріїв та партнерство, якому віддають перевагу
		українські та міжнародні компанії. «Сингенту» вирізняє високоякісна
		продукція, інтегрований підхід до вирішення поставлених завдань та
		використання переваг сільського господарства України.</p>
		<p class="style2">&nbsp;</p>
		<p class="style5"><strong>Головні напрямки компанії</strong></p>
		<p class="style1">&nbsp;</p>
		<p class="style2">
		<img src="http://www3.syngenta.com/country/ua/uk/aboutcompany/PublishingImages/KSV_2015/ksv_5_.jpg" width="760" height="10"></p>
		<p class="style1">&nbsp;</p>
				<table style="width: 760px">
            <tbody>
                <tr>
                    <td><a href="http://www3.syngenta.com/country/ua/uk/seeds/fieldcrops/Pages/fieldcrop.aspx" target="_blank"><img alt="" onmouseover="this.src='http://www3.syngenta.com/country/ua/uk/aboutcompany/PublishingImages/KSV_2015/ksv_3_.jpg'" onmouseout="this.src='http://www3.syngenta.com/country/ua/uk/aboutcompany/PublishingImages/KSV_2015/ksv_3.jpg'" style="border-top: 0px solid; border-right: 0px solid; border-bottom: 0px solid; border-left: 0px solid" src="http://www3.syngenta.com/country/ua/uk/aboutcompany/PublishingImages/KSV_2015/ksv_3.jpg"></a></td>
					<td><a href="http://www3.syngenta.com/country/ua/UK/CROPPROTECTION/CROPPROTECTION/Pages/home.aspx" target="_blank"><img alt="" onmouseover="this.src='http://www3.syngenta.com/country/ua/uk/aboutcompany/PublishingImages/KSV_2015/ksv_4_.jpg'" onmouseout="this.src='http://www3.syngenta.com/country/ua/uk/aboutcompany/PublishingImages/KSV_2015/ksv_4.jpg'" style="border-top: 0px solid; border-right: 0px solid; border-bottom: 0px solid; border-left: 0px solid" src="http://www3.syngenta.com/country/ua/uk/aboutcompany/PublishingImages/KSV_2015/ksv_4.jpg"></a></td>

 				</tr>
            </tbody>
        </table>

		<p class="style1">&nbsp;</p>
		<p class="style1">&nbsp;</p>
		<p class="style1">&nbsp;</p>
		<p class="style5"><strong>Чи знаєте Ви, що…</strong></p>
		<p class="style1">&nbsp;</p>
		<p class="style1">
		<img src="http://www3.syngenta.com/country/ua/uk/aboutcompany/PublishingImages/KSV_2015/ksv_5_.jpg" width="760" height="10"></p>
		<p class="style1">&nbsp;</p>
		<p class="style1">Багато відомих продуктів, які ми вживаємо, виготовлено
		з рослин компанії «Сингента»?</p>
		<p class="style1">&nbsp;</p>
		<p class="style2">Вимоги до тестування нашої продукції суворіші ніж до
		тестування ліків. Щоб вивести продукт на ринок, ми витрачаємо в
		середньому 200 млн доларів та 10 років на 180 наукових дослідів.</p>
		<p class="style2">&nbsp;</p>
		<p class="style2">Використовуючи засоби захисту рослин від «Сингенти»,
		фермери щороку вирощують 4,4 млн тонн бавовни. Цього достатньо, щоб
		забезпечити кожного у світі 3-ма футболками та кожного другого мешканця
		планети парою джинсів.</p>
		<p class="style1">&nbsp;</p>
		<p class="style1">
		<img src="http://www3.syngenta.com/country/ua/uk/aboutcompany/PublishingImages/KSV_2015/ksv_2.jpg" width="760" height="476"></p>
		<p class="style1">&nbsp;</p>
		<p class="style1">&nbsp;</p>
		<p>&nbsp;</p>
		<p>&nbsp;</p>
		<p>&nbsp;</p>
		</td>
	</tr>
</tbody></table>

					</div>
					</div>

</td>
</div>
