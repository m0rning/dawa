<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Records';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="record-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Record', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            [
              'class' => 'yii\grid\DataColumn', // can be omitted, as it is the default
              'value' => function ($data) {
                  return mb_substr($data->title,0,50,"UTF-8")."..."; // $data['name'] for array data, e.g. using SqlDataProvider.
              },
            ],
            'img',
            [
              'class' => 'yii\grid\DataColumn', // can be omitted, as it is the default
              'value' => function ($data) {
                  return mb_substr($data->description,0,50,"UTF-8")."..."; // $data['name'] for array data, e.g. using SqlDataProvider.
              },
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
